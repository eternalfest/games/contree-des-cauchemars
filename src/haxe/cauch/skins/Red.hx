package cauch.skins;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import vault.ISkin;

class Red implements ISkin {
  public static var ID_ROUGEOYANT: Int = 48;

  public var id(default, null): Int = 1240;
  public var sprite(default, null): Null<String> = null;

  public function new() {}

  public function skin(mc: MovieClip, subId: Null<Int>): Void {
    mc.gotoAndStop("" + (ID_ROUGEOYANT + 1));
  }
}