package cauch;

import etwin.ds.FrozenArray;
import patchman.IPatch;
import vault.ISkin;
import cauch.skins.Blue;
import cauch.skins.Red;

@:build(patchman.Build.di())
class Items {
  @:diExport
  public var skins(default, null): FrozenArray<ISkin>;

  public function new() {
    this.skins = FrozenArray.of(
      new Blue(),
      new Red()
    );
  }
}