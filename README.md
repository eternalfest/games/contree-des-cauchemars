# Contree des Cauchemars

Clone with HTTPS:
```shell
git clone --recurse-submodules https://gitlab.com/eternalfest/games/contree-des-cauchemars.git
```

Clone with SSH:
```shell
git clone --recurse-submodules git@gitlab.com:eternalfest/games/contree-des-cauchemars.git
```
